package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    ImageButton result_ImageButton;
    ImageButton result_imageButton2;
    Button rollButton;
    Button clearButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result_ImageButton = findViewById(R.id.result_ImageView);
        rollButton = findViewById(R.id.roll_button);
        clearButton = findViewById(R.id.clear_button);
        result_imageButton2 = findViewById(R.id.result_ImageViewDado2);

        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numRandom = newRandom();
                int dau1 = numRandom;
                diceRoll(numRandom, result_ImageButton);
                numRandom = newRandom();
                int dau2 = numRandom;
                diceRoll(numRandom, result_imageButton2);
                if (jackpot(dau1, dau2)) {
                    Toast.makeText(MainActivity.this, "JACKPOT!", Toast.LENGTH_SHORT).show();
                }
                result_ImageButton.setVisibility(View.VISIBLE);
                result_imageButton2.setVisibility(View.VISIBLE);
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result_ImageButton.setVisibility(View.INVISIBLE);
                result_imageButton2.setVisibility(View.INVISIBLE);
            }
        });

        result_ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numRandom = newRandom();
                diceRoll(numRandom, result_ImageButton);
            }
        });

        result_imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numRandom = newRandom();
                diceRoll(numRandom, result_imageButton2);
            }
        });

    }
    public Boolean jackpot(int dau1, int dau2) {
        if (dau1 == 6 && dau2 == 6) {
            return true;
        } else {
            return false;
        }
    }

    public void diceRoll(int num, ImageButton dado) {
        if (num == 1) {
            dado.setImageResource(R.drawable.dice_1);
        } else if (num == 2) {
            dado.setImageResource(R.drawable.dice_2);
        } else if (num == 3) {
            dado.setImageResource(R.drawable.dice_3);
        } else if (num == 4) {
            dado.setImageResource(R.drawable.dice_4);
        } else if (num == 5) {
            dado.setImageResource(R.drawable.dice_5);
        } else if (num == 6) {
            dado.setImageResource(R.drawable.dice_6);
        }
    }

    public int newRandom() {
        return (int) (Math.random()*(7-1)+1);
    }
}